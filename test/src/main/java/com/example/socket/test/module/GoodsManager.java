package com.example.socket.test.module;

import com.example.ramcache.anno.Inject;
import com.example.ramcache.service.EntityCacheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author frank
 * @date 2018-10-17 9:50
 * @desc
 **/
@Component
public class GoodsManager {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Inject
    private EntityCacheService<String, Goods> accountCache;

    @PostConstruct
    void init() {
        Goods goods = accountCache.loadOrCreate("0002", id -> {
            return Goods.valueOf(id, "createTest2", "createDesc2", 1000);
        });
        goods.update(100);
    }

    public void setAccountCache(EntityCacheService<String, Goods> accountCache) {
        this.accountCache = accountCache;
    }
}
