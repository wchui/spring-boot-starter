package com.example.socket.core;

/**
 * 回应对象
 */
public class Response<T>{

	/**
	 * 构建没有附加信息体的回应对象实例
	 * @param message
	 * @param body
	 * @return
	 */
	public static <T> Response<T> valueOfMessage(Message message, T body) {
		Response<T> result = new Response<T>();
		result.header = message.getHeader();
		result.body = body;
		return result;
	}

	/**
	 * 构建有附加信息体的回应对象实例
	 * @param message
	 * @param body
	 * @return
	 */
	public static <T> Response<T> valueOf(Header message, T body) {
		Response<T> result = new Response<T>();
		result.header = message;
		result.body = body;
		return result;
	}

	/**
	 * 构建回应对象实例<br/>
	 * 用于通信层返回{@link Response}对象，在该情况下设置{@link Response#sn}/{@link Response#command}/{@link Response#state}
	 * 均无效，这些值会由通信处理器进行设置。
	 * @param <T>
	 * @param body 信息体
	 * @return
	 */
	public static <T> Response<T> valueOf(T body) {
		Response<T> result = new Response<T>();
		result.body = body;
		return result;
	}
	/** 回应内容 */
	private T body;
	private Header header;

	public Header getHeader() {
		return header;
	}

	/**
	 * 添加状态
	 * @param added 被添加的状态
	 */
	public void addState(short added) {
		header.addState(added);
	}

	/**
	 * 移除状态
	 * @param removed 被移除的状态
	 */
	public void removeState(short removed) {
		header.removeState(removed);
	}

	/**
	 * 检查是否有指定状态
	 * @param check 被检查的状态
	 * @return
	 */
	public boolean hasState(int check) {
		return header.hasState((short)check);
	}

	/**
	 * 检查请求是否不正确
	 * @return
	 */
	public boolean hasError() {
		return header.hasState(MessageConstant.STATE_ERROR);
	}

	// Getter and Setter...

	public long getSn() {
		return header.getSn();
	}

	public long getSession() {
		return header.getSession();
	}

	public Command getCommand() {
		return header.getCommand();
	}

	public T getBody() {
		return body;
	}

	public int getState() {
		return header.getState();
	}

	public void setState(short state) {
		header.setState(state);
	}
}