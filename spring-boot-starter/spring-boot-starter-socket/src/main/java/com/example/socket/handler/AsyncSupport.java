package com.example.socket.handler;

import com.example.socket.thread.NamedThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author frank
 * 异步请求处理支持类
 */
public class AsyncSupport {

    private final static Logger logger = LoggerFactory.getLogger(AsyncSupport.class);

    private ExecutorService pool = new ThreadPoolExecutor(4, 128, 1, TimeUnit.MINUTES,
            new LinkedBlockingQueue<>(), new NamedThreadFactory("ASYNC"));

    /**
     * 通过同步队列执行任务
     * @param key 同步键
     * @param task 任务
     */
    public void execute(Runnable task) {
        try {
            pool.submit(task);
        } catch (Exception e) {
            logger.error("ASYNC", e);
        }
    }

}
