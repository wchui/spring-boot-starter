package com.example.common.test.ramcache;

import com.example.common.test.ramcache.manager.Player;
import com.example.common.test.ramcache.manager.PlayerManager;
import com.example.console.anno.ConsoleBean;
import com.example.console.anno.ConsoleCommand;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
@ConsoleBean
public class PlayerService {

    @Resource
    private PlayerManager playerManager;

    @ConsoleCommand(name = "cache",description = "测试缓存")
    public void testCache(int id, String args) {
        Player player = playerManager.load(id);
        playerManager.update(player, args);
        System.out.println("修改玩家信息成功");
    }

}
