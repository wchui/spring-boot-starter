package com.example.common.test.socket;

import com.example.common.test.socket.vo.TestReq;
import com.example.common.test.socket.vo.TestVo;
import com.example.socket.anno.SocketCommand;
import com.example.socket.anno.SocketModule;

@SocketModule(value = 1)
public interface TestFacade {
    @SocketCommand(1)
    public TestVo test(TestReq req);
}
