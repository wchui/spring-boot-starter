package com.example.socket.core;

/**
 * 会话属性键值常量
 * @author Ramon
 */
public interface SessionKeys {

    // ----- KEYS -----
    /** 管理后台标识属性 */
    String MANAGEMENT = "management";
    /** 用户身份标识键 */
    String IDENTITY = "identity";
    /** 身份处理完成标记键 */
    String PROCEED = "proceed";
    /** 忽略事件通知的标记键 */
    String IGNORE_EVENT = "ignore_event";
    /**游戏服id集合*/
    String SERVERS = "servers";

    // ----- ATTRIBUTES -----

    /** 管理后台会话属性 */
    Attribute<String> ATT_MANAGEMENT = new Attribute<>(SessionKeys.MANAGEMENT);
    /** 身份处理完成标记的会话属性 */
    Attribute<Boolean> ATT_PROCEED = new Attribute<>(SessionKeys.PROCEED);
    /** 忽略事件通知的会话属性 */
    Attribute<Boolean> ATT_IGNORE_EVENT = new Attribute<>(SessionKeys.IGNORE_EVENT);
    /** 被踢下线的会话属性 */
    Attribute<Boolean> ATT_KICKED = new Attribute<>("kicked");

}
