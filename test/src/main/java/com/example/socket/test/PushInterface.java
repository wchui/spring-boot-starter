package com.example.socket.test;

import com.example.socket.anno.SocketCommand;
import com.example.socket.anno.SocketModule;

/**
 * @author frank
 * @date 2019-03-14 18:21
 * @desc
 **/
@SocketModule(0)
public interface PushInterface {

    @SocketCommand(-1)
    void kick();
}
