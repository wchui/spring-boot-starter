package com.example.socket.test.module;

import com.example.ramcache.IEntity;
import com.example.ramcache.anno.Cached;
import com.example.ramcache.anno.CachedSizes;
import com.example.ramcache.anno.Persister;
import com.example.ramcache.anno.Persisters;
import com.example.ramcache.enhance.Enhance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author frank
 * 订单记录实体
 */
@Entity
@Table(catalog = "ramcache")
@Cached(size = CachedSizes.DEFAULT, persister = @Persister(Persisters.PRE_MINUTE))
public class Goods implements IEntity<String> {

    @Id
    /** 商品id*/
    @Column(length = 100)
    private String id;

    /** 商品名*/
    private String name;

    /** 描述*/
    private String des;

    /** 分*/
    private int money;

    public static Goods valueOf(String id, String name, String des, int money) {
        Goods goods = new Goods();
        goods.id = id;
        goods.name = name;
        goods.des = des;
        goods.money = money;
        return goods;
    }

    @Enhance
    public void update(int change) {
        money += change;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }
}