package com.example.socket.client;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author frank
 * 发送代理工厂
 */
public class SendProxyFactory {

	private ConcurrentHashMap<Class<?>, Object> caches = new ConcurrentHashMap<>();

	private InvocationHandler invoker;

	public SendProxyFactory(Client client, int timeout) {
		invoker = new SendProxyInvoker<Object>(client, timeout);
	}

	public <T> T getProxy(Class<T> clz) {
		T obj = (T) caches.get(clz);
		if (obj != null) {
			return obj;
		}
		T inst = (T) Proxy.newProxyInstance(clz.getClassLoader(), new Class[] { clz }, invoker);
		caches.putIfAbsent(clz, inst);
		return inst;
	}

}
