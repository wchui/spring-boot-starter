package com.example.event.rocket;

import com.alibaba.fastjson.JSON;

/**
 * @author frank
 * @date 2019-11-11 16:05
 * @desc
 **/
public class RemoteEvent {

    private String body;
    private String clazz;

    public RemoteEvent() {
    }

    public RemoteEvent(Object body) {
        this.clazz = body.getClass().getName();
        this.body = JSON.toJSONString(body);
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
