package com.example.socket.filter.firewall;

/***
 * 管理端ip matcher
 * @author frank
 */
public interface ManagementMatcher {

	boolean match(String ip);
}
