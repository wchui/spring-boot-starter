package com.example.socket.parameter;

import com.example.socket.core.Request;
import com.example.socket.core.Session;

/**
 * 参数对象,负责获取指定注释({@link InBody} / {@link InRequest} / {@link InSession})声明的参数
 */
public interface Parameter {

    /**
     * 获取对应的参数类型
     * @return
     */
    ParameterKind getKind();

    /**
     * 获取参数值
     * @param request
     * @param session
     * @param callback
     * @return
     */
    Object getValue(Request<?> request, Session session, ResultCallback<?> callback);


}
