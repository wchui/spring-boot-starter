package com.example.socket.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author frank
 * @date 2019-03-11 11:24
 * @desc
 **/
@ConfigurationProperties(prefix = "socket.client")
public class ClientProperties {
    /** socket receive size*/
    private int bufferRead = 1024;
    /** socket send size*/
    private int bufferWrite = 1024;
    /** wait response timout (seconds)*/
    private int responseTimeout = 120;
    /** default server address*/
    private String defaultAddress;
    /** none-active client remove time (seconds)*/
    private int nonActiveRemove = 600;
    /** message send failed retry interval*/
    private int messageResendInterval = 240;
    /** Message send failed save path */
    private String messageSaveFilePath = "message.history";
    /** heartbeat interval*/
    private int heartbeatInterval = 1800;
    /** heartbeat command*/
    private short heartbeatCommand = 100;
    /** heartbeat Module*/
    private short heartbeatModule = -100;
    /** io thread count*/
    private int coreThreadCount = 1;
    /** handler thread count*/
    private int handlerThreadCount = 2;
    /** connection failed retry times*/
    private int connectionFailRetryTimes = 10;

    public int getBufferRead() {
        return bufferRead;
    }

    public void setBufferRead(int bufferRead) {
        this.bufferRead = bufferRead;
    }

    public int getBufferWrite() {
        return bufferWrite;
    }

    public void setBufferWrite(int bufferWrite) {
        this.bufferWrite = bufferWrite;
    }

    public int getResponseTimeout() {
        return responseTimeout;
    }

    public void setResponseTimeout(int responseTimeout) {
        this.responseTimeout = responseTimeout;
    }

    public String getDefaultAddress() {
        return defaultAddress;
    }

    public void setDefaultAddress(String defaultAddress) {
        this.defaultAddress = defaultAddress;
    }

    public int getNonActiveRemove() {
        return nonActiveRemove;
    }

    public void setNonActiveRemove(int nonActiveRemove) {
        this.nonActiveRemove = nonActiveRemove;
    }

    public int getMessageResendInterval() {
        return messageResendInterval;
    }

    public void setMessageResendInterval(int messageResendInterval) {
        this.messageResendInterval = messageResendInterval;
    }

    public String getMessageSaveFilePath() {
        return messageSaveFilePath;
    }

    public void setMessageSaveFilePath(String messageSaveFilePath) {
        this.messageSaveFilePath = messageSaveFilePath;
    }

    public int getHeartbeatInterval() {
        return heartbeatInterval;
    }

    public void setHeartbeatInterval(int heartbeatInterval) {
        this.heartbeatInterval = heartbeatInterval;
    }

    public short getHeartbeatCommand() {
        return heartbeatCommand;
    }

    public void setHeartbeatCommand(short heartbeatCommand) {
        this.heartbeatCommand = heartbeatCommand;
    }

    public short getHeartbeatModule() {
        return heartbeatModule;
    }

    public void setHeartbeatModule(short heartbeatModule) {
        this.heartbeatModule = heartbeatModule;
    }

    public int getCoreThreadCount() {
        return coreThreadCount;
    }

    public void setCoreThreadCount(int coreThreadCount) {
        this.coreThreadCount = coreThreadCount;
    }

    public int getHandlerThreadCount() {
        return handlerThreadCount;
    }

    public void setHandlerThreadCount(int handlerThreadCount) {
        this.handlerThreadCount = handlerThreadCount;
    }

    public int getConnectionFailRetryTimes() {
        return connectionFailRetryTimes;
    }

    public void setConnectionFailRetryTimes(int connectionFailRetryTimes) {
        this.connectionFailRetryTimes = connectionFailRetryTimes;
    }
}
