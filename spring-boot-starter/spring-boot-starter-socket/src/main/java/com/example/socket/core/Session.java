package com.example.socket.core;

import io.netty.util.AttributeKey;

import java.net.SocketAddress;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 通信会话抽象接口，用于屏蔽底层的真实会话对象
 */
public interface Session {

    /**
     * 当前业务线程的SESSION
     */
    ThreadLocal<Session> THREAD_LOCAL = new ThreadLocal<>();

    AttributeKey<Session> SESSION_KEY = AttributeKey.valueOf("SESSION");
    AttributeKey<Boolean> MANAGEMENT_KEY = AttributeKey.valueOf("MANAGEMENT");
    AttributeKey<Boolean> KCP_BOOS = AttributeKey.valueOf("KCP_BOOS");
    AttributeKey<Boolean> KCP_WORK_REGIST = AttributeKey.valueOf("KCP_WORK_REGIST");
    AttributeKey<Long> ACCESS_TIME = AttributeKey.valueOf("accessTime");
    AttributeKey<AtomicInteger> INVALID_TIMES = AttributeKey.valueOf("invalid_times");

    /**
     * SESSION ID
     */
    long getId();

    /**
     * 获取SESSION上下文
     */
    SessionContext getContext();

    /**
     * 连接是否有效
     */
    boolean isConnected();

    /**
     * 连接的远端地址
     */
    SocketAddress getRemoteAddress();

    /**
     * 关闭当前通信会话
     */
    Future<?> close();

    /**
     * 向当前通信会话推送信息
     * @param msg
     */
    Future<?> write(Object msg);

    /**
     * 销毁当前通信会话
     */
    void destory();

    /**
     * 是否短链接
     */
    boolean isTransitory();

    SessionContext bindContext(SessionContext context);

    void setTransitory(boolean transitory);

    boolean isDestroyed();

    /**seesion上一次数据读写时间*/
    long getLastTime();
}