package com.example.ramcache.properties;

import com.example.ramcache.persist.PersisterType;

import java.util.Objects;

/**
 * @author frank
 * @date 2019-03-26 14:33
 * @desc
 **/
public class PersisterProperties {

    private String name;
    private String config;
    private PersisterType type;

    public static PersisterProperties valueOf(String name, String config, PersisterType type) {
        PersisterProperties persisterProperties = new PersisterProperties();
        persisterProperties.name = name;
        persisterProperties.config = config;
        persisterProperties.type = type;
        return persisterProperties;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public PersisterType getType() {
        return type;
    }

    public void setType(PersisterType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PersisterProperties that = (PersisterProperties) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(config, that.config) &&
                type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, config, type);
    }
}
