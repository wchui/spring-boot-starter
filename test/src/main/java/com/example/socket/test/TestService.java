package com.example.socket.test;

import com.example.socket.client.Client;
import com.example.socket.client.ClientFactory;
import com.example.socket.client.SocketCallbackTemplate;
import com.example.socket.core.Command;
import com.example.socket.core.Request;
import com.example.socket.core.Response;
import com.example.console.anno.ConsoleBean;
import com.example.console.anno.ConsoleCommand;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author frank
 * @date 2019-03-14 17:57
 * @desc
 **/
@Component
@ConsoleBean
public class TestService {
    @Resource
    private ClientFactory clientFactory;

    @ConsoleCommand(name = "send", description = "测试发送消息！")
    public void send() {
        Client client = clientFactory.getClient(true);
        Command commond = Command.valueOf((short) 0, (short) 1);
        Request<SocketFacade.Req> request = Request.valueOf(commond, new SocketFacade.Req("张三", 20));
        client.send(request, new SocketCallbackTemplate<Response<SocketFacade.Req>>() {
            @Override
            public void onSuccess(Response<SocketFacade.Req> response) {
                System.out.println(response.getBody());
                //client.close();
            }
        }, false);
    }
}
