package com.example.common.test.socket;

import com.example.common.test.socket.vo.TestReq;
import com.example.common.test.socket.vo.TestVo;
import com.example.console.anno.ConsoleBean;
import com.example.console.anno.ConsoleCommand;
import com.example.socket.client.Client;
import com.example.socket.client.ClientFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@ConsoleBean
@Component
public class TestFacadeImpl implements TestFacade {

    @Resource
    private ClientFactory clientFactory;


    public TestVo test(TestReq req) {
        System.out.println(req);
        TestVo vo = new TestVo();
        vo.setAge(110);
        vo.setOther("other");
        return vo;
    }

    @ConsoleCommand(name = "send", description = "测试socket")
    public void testSocket() {
        try {
            Client client = clientFactory.getClient("127.0.0.1:9999", true);
            TestFacade facade = client.getSendProxy(TestFacade.class);
            TestReq req = new TestReq();
            req.setId(10);
            req.setName("frank");
            TestVo vo = facade.test(req);
            System.out.println(vo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
