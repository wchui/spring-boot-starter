package com.example.socket.handler;

import com.example.socket.core.Command;

import java.util.Objects;

/**
 * 指令信息对象
 *
 * @author frank
 */
public class CommandInfo {

    /** 指令对象 */
    private final Command command;
    /** 消息体类型定义 */
    private final TypeDefinition definition;
    /** 指令处理器 */
    private final Processor<?, ?> processor;

    /**
     * 构造方法
     * @param command 指令对象(必须)
     * @param definition 消息体类型定义(必须)
     * @param processor 指令处理器(可选)
     * @throws IllegalArgumentException 指令或消息体为空时抛出
     */
    public CommandInfo(Command command, TypeDefinition definition, Processor<?, ?> processor) {
        if (command == null || definition == null) {
            throw new IllegalArgumentException("指令或消息体定义不能为空");
        }
        this.command = command;
        this.definition = definition;
        this.processor = processor;
    }

    /**
     * 检查是否有指令处理器
     * @return true:有, false:没有
     */
    public boolean hasProcessor() {
        return processor != null;
    }

    /**
     * 获取指令对象
     * @return
     */
    public Command getCommand() {
        return command;
    }

    /**
     * 获取消息体类型定义
     * @return
     */
    public TypeDefinition getDefinition() {
        return definition;
    }

    /**
     * 获取指令处理器
     * @return
     */
    public Processor getProcessor() {
        return processor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CommandInfo that = (CommandInfo) o;
        return Objects.equals(command, that.command);
    }

    @Override
    public int hashCode() {
        return Objects.hash(command);
    }

    @Override
    public String toString() {
        return "指令:" + command + " 消息体:" + definition + " 处理器:" + (processor == null ? "不存在" : "存在");
    }
}
