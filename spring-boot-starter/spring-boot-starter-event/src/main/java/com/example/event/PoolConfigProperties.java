package com.example.event;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author frank
 * @date 2019-03-22 18:16
 * @desc
 **/
@ConfigurationProperties(prefix = "event.pool")
public class PoolConfigProperties {

    private Integer size = 5;
    private Integer MaxSize = 10;
    private Integer KeepAlive = 60;
    private Integer AwaitTime = 60;
    private Integer queueSize = 32768;

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getMaxSize() {
        return MaxSize;
    }

    public void setMaxSize(Integer maxSize) {
        MaxSize = maxSize;
    }

    public Integer getKeepAlive() {
        return KeepAlive;
    }

    public void setKeepAlive(Integer keepAlive) {
        KeepAlive = keepAlive;
    }

    public Integer getAwaitTime() {
        return AwaitTime;
    }

    public void setAwaitTime(Integer awaitTime) {
        AwaitTime = awaitTime;
    }

    public Integer getQueueSize() {
        return queueSize;
    }

    public void setQueueSize(Integer queueSize) {
        this.queueSize = queueSize;
    }
}
