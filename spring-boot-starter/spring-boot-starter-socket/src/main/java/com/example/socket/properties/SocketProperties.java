package com.example.socket.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author frank
 * @date 2019-03-13 15:54
 * @desc
 **/
@ConfigurationProperties(prefix = "socket")
public class SocketProperties {

    /** server config*/
    private ServerProperties server = new ServerProperties();
    /** client config*/
    private ClientProperties client = new ClientProperties();

    public ServerProperties getServer() {
        return server;
    }

    public void setServer(ServerProperties server) {
        this.server = server;
    }

    public ClientProperties getClient() {
        return client;
    }

    public void setClient(ClientProperties client) {
        this.client = client;
    }
}
