package com.example.event.rocket;

import org.springframework.boot.context.properties.EnableConfigurationProperties;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

/**
 * @author frank
 * @date 2019-11-11 16:01
 * @desc
 **/
@EnableConfigurationProperties(EventProperties.class)
public class RemoteEventBus {

    @Resource
    private EventProperties eventProperties;

    @Resource
    private RocketMqProducer rocketMqProducer;

    public List<Future<?>> post(Object event) throws Exception {
        rocketMqProducer.pushMessage(MqMessage.valueOf(eventProperties.getTopic(), eventProperties.getTag(), new RemoteEvent(event)));
        return new ArrayList<>();
    }


}
