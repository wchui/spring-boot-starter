package com.example.socket.anno;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 推送接口声明<br>
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SocketPush {
	/**
	 * 当使用短连接时是否开启模块延迟推送<br>
	 * 不使用短连接时该配置无效
	 */
	boolean delayed() default true;
}
