package com.example.socket.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author frank
 * @date 2019-03-09 14:13
 * @desc
 **/
@ConfigurationProperties(prefix = "socket.server")
public class ServerProperties {

    /** host:port*/
    private String bind = ":6000";
    /** auto start server on spring initializer*/
    private boolean autoStart = true;
    /** socket receive size*/
    private int bufferRead = 1024;
    /** socket send size*/
    private int bufferWrite = 1024;
    /** socket channel timeout (seconds)*/
    private int timeout = 180;
    /** socket max connection*/
    private int backlog = 10000;
    /** tcp no delay*/
    private boolean tcpNodelay = true;
    /** socket half connection*/
    private boolean allowHalfClosure = false;
    /** pooled buffer*/
    private boolean pooledBuffer = false;
    /** use custom thread pool*/
    private boolean customPool = false;
    /** custom pool buffer size*/
    private int customPoolBufferSize = 4096;
    /** custom pool size*/
    private int customPoolSize = 4;
    /** session destroy delay (passivation time)*/
    private int sessionDelayDestroy = 600;
    /** transitory connection*/
    private boolean transitory = true;
    /** firewall config*/
    private FirewallProperties firewall = new FirewallProperties();
    /** filter config*/
    private FilterProperties filter = new FilterProperties();

    public String getBind() {
        return bind;
    }

    public void setBind(String bind) {
        this.bind = bind;
    }

    public boolean isAutoStart() {
        return autoStart;
    }

    public void setAutoStart(boolean autoStart) {
        this.autoStart = autoStart;
    }

    public int getBufferRead() {
        return bufferRead;
    }

    public void setBufferRead(int bufferRead) {
        this.bufferRead = bufferRead;
    }

    public int getBufferWrite() {
        return bufferWrite;
    }

    public void setBufferWrite(int bufferWrite) {
        this.bufferWrite = bufferWrite;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getBacklog() {
        return backlog;
    }

    public void setBacklog(int backlog) {
        this.backlog = backlog;
    }

    public boolean isTcpNodelay() {
        return tcpNodelay;
    }

    public void setTcpNodelay(boolean tcpNodelay) {
        this.tcpNodelay = tcpNodelay;
    }

    public boolean isAllowHalfClosure() {
        return allowHalfClosure;
    }

    public void setAllowHalfClosure(boolean allowHalfClosure) {
        this.allowHalfClosure = allowHalfClosure;
    }

    public boolean isPooledBuffer() {
        return pooledBuffer;
    }

    public void setPooledBuffer(boolean pooledBuffer) {
        this.pooledBuffer = pooledBuffer;
    }

    public int getCustomPoolBufferSize() {
        return customPoolBufferSize;
    }

    public void setCustomPoolBufferSize(int customPoolBufferSize) {
        this.customPoolBufferSize = customPoolBufferSize;
    }

    public int getCustomPoolSize() {
        return customPoolSize;
    }

    public void setCustomPoolSize(int customPoolSize) {
        this.customPoolSize = customPoolSize;
    }

    public boolean isCustomPool() {
        return customPool;
    }

    public void setCustomPool(boolean customPool) {
        this.customPool = customPool;
    }

    public int getSessionDelayDestroy() {
        return sessionDelayDestroy;
    }

    public void setSessionDelayDestroy(int sessionDelayDestroy) {
        this.sessionDelayDestroy = sessionDelayDestroy;
    }

    public boolean isTransitory() {
        return transitory;
    }

    public void setTransitory(boolean transitory) {
        this.transitory = transitory;
    }

    public FirewallProperties getFirewall() {
        return firewall;
    }

    public void setFirewall(FirewallProperties firewall) {
        this.firewall = firewall;
    }

    public FilterProperties getFilter() {
        return filter;
    }

    public void setFilter(FilterProperties filter) {
        this.filter = filter;
    }

}
