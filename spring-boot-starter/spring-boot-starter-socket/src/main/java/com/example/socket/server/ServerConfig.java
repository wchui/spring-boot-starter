package com.example.socket.server;

import com.example.socket.properties.ServerProperties;
import com.example.socket.utils.IpUtils;

import java.net.InetSocketAddress;

/**
 * 服务器配置信息对象
 * @author frank
 */
public class ServerConfig {

    private SessionConfig sessionConfig;
    private ExecutorConfig executorConfig;
    /**是否使用池化buff*/
    private boolean pooled;
    /** 自动启动 */
    private boolean autoStart;
    /**服务器port*/
    private int port;
    private InetSocketAddress address;

    public ServerConfig(ServerProperties properties) {
        this.address = IpUtils.toInetSocketAddress(properties.getBind());
        this.pooled = properties.isPooledBuffer();
        this.autoStart = properties.isAutoStart();

        /** 配置socket相关参数*/
        int readBufferSize = properties.getBufferRead();
        int writeBuffSize = properties.getBufferWrite();
        int timeout = properties.getTimeout();
        int backlog = properties.getBacklog();
        boolean tcpNodelay = properties.isTcpNodelay();
        boolean allowHalfClosure = properties.isAllowHalfClosure();
        this.sessionConfig = SessionConfig.valueOfServer(readBufferSize, writeBuffSize, timeout, backlog, tcpNodelay, allowHalfClosure, pooled);

        /** 配置自定义线程池*/
        boolean customPool = properties.isCustomPool();
        int poolSize = properties.getCustomPoolSize();
        int poolBufferSize = properties.getCustomPoolBufferSize();
        executorConfig = ExecutorConfig.valueOf(poolSize, poolBufferSize, customPool);
    }

    public boolean isPooled() {
        return pooled;
    }

    /**
     * 是否自动启动
     * @return
     */
    public boolean isAutoStart() {
        return autoStart;
    }

    // Getter and Setter ...

    public SessionConfig getSessionConfig() {
        return sessionConfig;
    }

    public ExecutorConfig getExecutorConfig() {
        return executorConfig;
    }

    public void setSessionConfig(SessionConfig sessionConfig) {
        this.sessionConfig = sessionConfig;
    }

    public void setExecutorConfig(ExecutorConfig executorConfig) {
        this.executorConfig = executorConfig;
    }

    public void setPooled(boolean pooled) {
        this.pooled = pooled;
    }

    public void setAutoStart(boolean autoStart) {
        this.autoStart = autoStart;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public InetSocketAddress getAddress() {
        return address;
    }

    public void setAddress(InetSocketAddress address) {
        this.address = address;
    }
}
