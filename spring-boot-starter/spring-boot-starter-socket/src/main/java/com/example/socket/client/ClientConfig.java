package com.example.socket.client;

import com.example.socket.core.Command;
import com.example.socket.core.Header;
import com.example.socket.core.Message;
import com.example.socket.properties.ClientProperties;
import com.example.socket.server.SessionConfig;

/**
 * @author frank
 * 服务器配置信息对象
 */
public class ClientConfig {

    private SessionConfig sessionConfig;
    /** 默认地址 */
    private String defaultAddress;
    /** 回应超时时间(毫秒) */
    private int responseTimeout;
    /** 客户端过期时间(秒) */
    private int removeTimes;
    /**消息重发时间 单位秒*/
    private int resendTime;
    /**失败消息存储文件路径*/
    private String messageFile;
    /**心跳包间隔时间*/
    private int heartBeatInterval;
    /**心跳包请求*/
    private Message heartBeat;
    /**客户端业务处理核心线程*/
    private int clientThreadsCore;
    /**客户端业务处理核心线程*/
    private int clientIoCore;
    /**客户端重试次数*/
    private int maxRetry;

    public ClientConfig(ClientProperties properties) {
        this.defaultAddress = properties.getDefaultAddress();
        this.responseTimeout = properties.getResponseTimeout() * 1000;
        this.removeTimes = properties.getNonActiveRemove();
        this.resendTime = properties.getMessageResendInterval();
        this.messageFile = properties.getMessageSaveFilePath();
        this.heartBeatInterval = properties.getHeartbeatInterval();
        Command cmd = Command.valueOf(properties.getHeartbeatModule(), properties.getHeartbeatCommand());
        this.heartBeat = Message.valueOf(Header.valueOf((byte) 0, (short) 0, 0, (long) -1, cmd), null);
        this.clientThreadsCore = properties.getHandlerThreadCount();
        this.clientIoCore = properties.getCoreThreadCount();
        this.maxRetry = properties.getConnectionFailRetryTimes();
        int readBufferSize = properties.getBufferRead();
        int writeBufferSize = properties.getBufferWrite();
        this.sessionConfig = SessionConfig.valueOfClient(readBufferSize, writeBufferSize);
    }

    // Getter and Setter ...

    public int getHeartBeatInterval() {
        return heartBeatInterval;
    }

    public SessionConfig getSessionConfig() {
        return sessionConfig;
    }

    public int getResponseTimeout() {
        return responseTimeout;
    }

    public int getRemoveTimes() {
        return removeTimes;
    }

    public String getDefaultAddress() {
        return defaultAddress;
    }

    public int getResendTime() {
        return resendTime;
    }

    public String getMessageFile() {
        return messageFile;
    }

    public Message getHeartBeat() {
        return heartBeat;
    }

    public int getClientThreadsCore() {
        return clientThreadsCore;
    }

    public int getClientIoCore() {
        return clientIoCore;
    }

    public void setSessionConfig(SessionConfig sessionConfig) {
        this.sessionConfig = sessionConfig;
    }

    public void setDefaultAddress(String defaultAddress) {
        this.defaultAddress = defaultAddress;
    }

    public void setResponseTimeout(int responseTimeout) {
        this.responseTimeout = responseTimeout;
    }

    public void setRemoveTimes(int removeTimes) {
        this.removeTimes = removeTimes;
    }

    public void setResendTime(int resendTime) {
        this.resendTime = resendTime;
    }

    public void setMessageFile(String messageFile) {
        this.messageFile = messageFile;
    }

    public void setHeartBeatInterval(int heartBeatInterval) {
        this.heartBeatInterval = heartBeatInterval;
    }

    public void setHeartBeat(Message heartBeat) {
        this.heartBeat = heartBeat;
    }

    public void setClientThreadsCore(int clientThreadsCore) {
        this.clientThreadsCore = clientThreadsCore;
    }

    public void setClientIoCore(int clientIoCore) {
        this.clientIoCore = clientIoCore;
    }

    public int getMaxRetry() {
        return maxRetry;
    }

    public void setMaxRetry(int maxRetry) {
        this.maxRetry = maxRetry;
    }
}
