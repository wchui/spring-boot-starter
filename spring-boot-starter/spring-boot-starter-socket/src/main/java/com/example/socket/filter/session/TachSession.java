package com.example.socket.filter.session;

import io.netty.channel.Channel;

public interface TachSession {

	// ---------- SESSION ----------

	/**
	 * 分离 SESSION
	 */
	void detach(Channel channel);

	/**
	 * 附加 SESSION
	 */
	void attach(Channel channel, boolean first);

}
