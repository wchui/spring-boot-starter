package com.example.socket.anno.impl;

import com.example.socket.core.Request;
import com.example.socket.core.Session;
import com.example.socket.parameter.Parameter;
import com.example.socket.parameter.ParameterKind;
import com.example.socket.parameter.ResultCallback;

public class SessionParameter implements Parameter {

	public static final SessionParameter instance = new SessionParameter();

	@Override
	public ParameterKind getKind() {
		return ParameterKind.SESSION;
	}

	@Override
	public Object getValue(Request<?> request, Session session, ResultCallback<?> callback) {
		return session;
	}

}
