package com.example.socket.client;

import com.example.socket.core.Message;
import com.example.socket.thread.DelayedElement;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Date;

public class MessageResend extends DelayedElement<Message> {

	public MessageResend(Message content, Date end) {
		super(content, end);
	}
	
	public MessageResend(){
		super(null, null);
	}
	/**目标地址*/
	private String address;
	
	private int clientType;
	
	public static MessageResend valueOf(int type, String address, Message message, Date end){
		MessageResend result = new MessageResend(message, end);
		result.address = address;
		result.clientType = type;
		return result;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	@JsonDeserialize(using=String2MessageDeserializer.class)
	public void setContent(Message content) {
		super.setContent(content);
	}

	public int getClientType() {
		return clientType;
	}

	public void setClientType(int clientType) {
		this.clientType = clientType;
	}	
}