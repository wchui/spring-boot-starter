package com.example.socket.handler;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 序列号生成器，用于生成以1开始的正整数，当到达最大值将重新从1开始自增
 * 负数
 */
public class SnGenerator {

	/** 开始值 */
	private static final int START = 0;

	private AtomicInteger sequence = new AtomicInteger(START);

	/**
	 * 获取下一个序列号
	 * @return
	 */
	public int next() {
		int result = sequence.decrementAndGet();
		if (result <= Integer.MIN_VALUE) {
			sequence.set(START);
		}
		return result;
	}
}