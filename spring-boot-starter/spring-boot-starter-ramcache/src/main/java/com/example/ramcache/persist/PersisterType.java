package com.example.ramcache.persist;

public enum PersisterType {
	
	/** 定时 */
	TIMING,
	/** 队列 */
	QUEUE;

}
