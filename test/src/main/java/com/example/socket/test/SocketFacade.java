package com.example.socket.test;

import com.example.socket.anno.SocketCommand;
import com.example.socket.anno.SocketModule;
import com.example.socket.core.Session;
import com.example.socket.filter.session.SessionManager;
import com.example.console.anno.ConsoleBean;
import com.example.console.anno.ConsoleCommand;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author frank
 * @date 2019-03-09 12:21
 * @desc
 **/
@Component
@SocketModule(0)
@ConsoleBean
public class SocketFacade {


    @Resource
    private SessionManager sessionManager;



    @ConsoleCommand(name = "test", description = "测试")
    @SocketCommand(1)
    public String login(Session session, Req s) {
        sessionManager.getPushProxy(PushInterface.class, session).kick();
        System.out.println("server received:" + s);
        return "server return :" + s;
    }

    /**
     * 测试发送的Vo
     */
    public static class Req {
        String name;
        int age;

        public Req(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public Req() {
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "Req{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }
}
