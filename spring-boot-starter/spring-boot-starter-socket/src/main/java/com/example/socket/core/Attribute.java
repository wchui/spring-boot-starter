package com.example.socket.core;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 属性类，用于统一与简化属性操作<br/>
 * 所有属性都将放在以{@link Session#MAIN_KEY}作为KEY，对应的{@link ConcurrentHashMap}中
 * @author frank
 */
@SuppressWarnings("unchecked")
public class Attribute<T> implements Serializable {

	private static final long serialVersionUID = -2820545255304851339L;

	/** 属性键 */
	private final String key;

	/**
	 * 构建属性对象
	 * @param key 属性键
	 */
	public Attribute(String key) {
		this.key = key;
	}

	/**
	 * 获取属性值
	 * @param session
	 * @return
	 */
	public T getValue(Session session) {
		SessionContext context = session.getContext();
		if (context == null) {
			return null;
		}
		return (T) context.get(key);
	}

	/**
	 * 获取属性值，不存在则以默认值返回
	 * @param session
	 * @param defaultValue 默认值
	 * @return
	 */
	public T getValue(Session session, T defaultValue) {
		if (contain(session)) {
			return getValue(session);
		}
		return defaultValue;
	}

	/**
	 * 设置属性值
	 * @param session
	 * @param value
	 * @return
	 */
	public T setValue(Session session, T value) {
		SessionContext context = session.getContext();
		if (context == null) {
			return null;
		}
		return (T) context.putIfAbsent(key, value);
	}

	/**
	 * 移除属性
	 * @param session
	 */
	public T remove(Session session) {
		SessionContext context = session.getContext();
		if (context == null) {
			return null;
		}
		return (T) context.remove(key);
	}

	/**
	 * 检查是否存在对应的属性值
	 * @param session
	 * @return
	 */
	public boolean contain(Session session) {
		SessionContext context = session.getContext();
		if (context == null) {
			return false;
		}
		return context.containsKey(key);
	}

	/**
	 * 获取属性键
	 * @return
	 */
	public String getKey() {
		return key;
	}

	@Override
	public String toString() {
		return key;
	}

}
