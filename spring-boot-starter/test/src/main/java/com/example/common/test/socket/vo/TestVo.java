package com.example.common.test.socket.vo;

import com.baidu.bjf.remoting.protobuf.annotation.Protobuf;
import com.baidu.bjf.remoting.protobuf.annotation.ProtobufClass;

@ProtobufClass
public class TestVo {
    @Protobuf
    private int age;

    @Protobuf
    private String other;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    @Override
    public String toString() {
        return "TestVo{" +
                "age=" + age +
                ", other='" + other + '\'' +
                '}';
    }
}
