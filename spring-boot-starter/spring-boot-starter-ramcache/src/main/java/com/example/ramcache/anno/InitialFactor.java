package com.example.ramcache.anno;

/**
 * 加载因子
 * @author yangjin
 * @date 2017年9月13日 下午4:19:35
 */
public enum InitialFactor {

	/**十分之一*/
	ONE_TENTH(0.1f),
	/**四分之一*/
	QUARTER(0.25f),
	/**五分之二*/
	TWO_FIFTHS(0.4f),
	/**三分之二*/
	TWO_THIRD(0.6f),
	
	;
	
	private float factor;
	
	private InitialFactor(float factor){
		this.factor = factor;
	}
	
	public float getFactor(){
		return factor;
	}
}
