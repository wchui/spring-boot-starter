package com.example.socket.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author frank
 * @date 2019-03-12 9:49
 * @desc
 **/
@ConfigurationProperties(prefix = "socket.server.filter")
public class FilterProperties {
    /** inner service*/
    private String allowIps = "127.0.0.1=local,192.168.*.*=intranet";

    public String getAllowIps() {
        return allowIps;
    }

    public void setAllowIps(String allowIps) {
        this.allowIps = allowIps;
    }
}
