package com.example.socket.core;

/**
 * {@link Message}的常量定义
 */
public interface MessageConstant {
	
	// 包定义部分
	/** 包长度 */
	int PACKAGE_LENGTH = 8;
	/** 用于推送的消息序号 */
	long DEFAULT_SN = -1;

	// 信息状态部分
	
	/** 状态:正常(请求状态) */
	short STATE_NORMAL = 0;

	/** 状态:回应(不是回应就是请求) */
	short STATE_RESPONSE = 1;

	/** 错误标记位(没有该状态代表正常) */
	short STATE_ERROR =1 << 2;
	
	/** 会话身份异常 */
	short IDENTITY_EXCEPTION = 1 << 3;
	

//	/** 请求指令不存在 */
//	short COMMAND_NOT_FOUND = 1 << 4;
//
//	/** 解码异常 */
//	short DECODE_EXCEPTION = 1 << 5;
//
//	/** 编码异常 */
//	short ENCODE_EXCEPTION = 1 << 6;
//
//	/** 参数异常 */
//	short PARAMETER_EXCEPTION = 1 << 7;
//
//	/** 处理异常 */
//	short PROCESSING_EXCEPTION = 1 << 8;
//
//
//	/** 消息SN异常 */
//	short MSG_SN_EXCEPTION = 1 << 9;
//
	/** 未知异常 */
	short UNKNOWN_EXCEPTION = 1 << 10;
	
	/**目标游戏服没找到*/
	short TARGET_SERVER_NOT_FOUND = 1<<11;
}
