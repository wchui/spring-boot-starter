package com.example.common.test.ramcache.manager;

import com.example.ramcache.IEntity;
import com.example.ramcache.anno.*;
import com.example.ramcache.enhance.Enhance;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Cached(size = CachedSizes.DEFAULT, persister = @Persister(Persisters.PRE_MINUTE), type= CacheType.LRU)
public class Player implements IEntity<Long> {

    @Id
    private long id;
    private String name;

    public static Player valueOf(long id) {
        Player test = new Player();
        test.id = id;
        return test;
    }

    @Enhance
    void update(String name){
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Player other = (Player) obj;
        if (id != other.id)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

}