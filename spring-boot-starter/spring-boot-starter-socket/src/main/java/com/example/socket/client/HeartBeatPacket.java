package com.example.socket.client;

import com.example.socket.thread.DelayedElement;

import java.util.Date;

public class HeartBeatPacket extends DelayedElement<String> {

    public int clientType;

    public HeartBeatPacket(int type, String content, Date end) {
        super(content, end);
        clientType = type;
    }

    public int getClientType() {
        return clientType;
    }
}