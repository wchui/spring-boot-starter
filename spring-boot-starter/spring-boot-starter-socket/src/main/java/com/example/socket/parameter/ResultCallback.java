package com.example.socket.parameter;

/**
 * 异步处理结果回调
 * @author ramon
 * @param <T>
 */
public interface ResultCallback<T> {

	/**
	 * 返回结果的异步回调
	 */
	void call(T result);
}
