package com.example.socket.codec.protobuf;

import java.util.HashSet;
import java.util.Set;

/**
 * @author frank
 * @date 2020-03-10 5:01 下午
 * @desc
 **/
public class ProtoFile {
	/**包名*/
	private String pkg;
	/**文件名*/
	private String fileName;
	/**需要导入的文件*/
	private Set<String> imports = new HashSet<>();
	/**头*/
	private StringBuilder header = new StringBuilder();
	/**body*/
	private StringBuilder body = new StringBuilder();
	
	public static ProtoFile valueOf(String name, String pkg){
		ProtoFile result = new ProtoFile();
		result.fileName = name;
		result.pkg = pkg;
		return result;
	}
	
	public void addImport(String fileName) {
		imports.add(fileName);
	}

	public String getPkg() {
		return pkg;
	}

	public String getFileName() {
		return fileName;
	}

	public Set<String> getImports() {
		return imports;
	}

	public StringBuilder getBody() {
		return body;
	}

	public void setPkg(String pkg) {
		this.pkg = pkg;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setImports(Set<String> imports) {
		this.imports = imports;
	}

	public void setBody(StringBuilder body) {
		this.body = body;
	}

	public StringBuilder getHeader() {
		return header;
	}

	public void setHeader(StringBuilder header) {
		this.header = header;
	}
}