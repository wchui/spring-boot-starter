package com.example.ramcache.orm;

import com.example.ramcache.IEntity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

/**
 * 实体的元信息
 * @author Frank
 */
public interface EntityMetadata {

	/**
	 * 获取完整的实体名
	 * @return
	 */
	String getFullName();
	
	/**
	 * 获取简短的实体名
	 * @return
	 */
	String getShortName();

	/**
	 * 获取实体的属性域与对应的类型
	 * @return
	 */
	Map<String, String> getFields();

	/**
	 * 获取实体的主键名
	 * @return
	 */
	String getPrimaryKey();

	/**
	 * 获取实体的索引键名
	 * @return
	 */
	Collection<String> getIndexKeys();

	/**
	 * 获取版本号属性名
	 * @return
	 */
	String getVersionKey();

	/**
	 * 获取主键的值类型
	 * @return
	 */
	<PK extends Serializable> Class<PK> getPrimaryKeyClass();

	/**
	 * 获取实体类
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	<T extends IEntity> Class<T> getEntityClass();

}
