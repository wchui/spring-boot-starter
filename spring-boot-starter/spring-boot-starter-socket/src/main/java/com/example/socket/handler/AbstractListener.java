package com.example.socket.handler;

import com.example.socket.core.Message;
import com.example.socket.core.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractListener implements Listener {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    /** 消息分发器 */
    protected Dispatcher dispatcher;

    // -----

    /** 消息分发器 */
    public Dispatcher getDispatcher() {
        return dispatcher;
    }

    /** 消息分发器 */
    @Override
    public void setDispatcher(Dispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    // -----

    @Override
    public void received(Message message, Session session) {
    }

    @Override
    public void sent(Message message, Session session) {
    }

}
