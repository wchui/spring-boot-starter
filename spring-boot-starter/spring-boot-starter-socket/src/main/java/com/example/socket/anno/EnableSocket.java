package com.example.socket.anno;

import java.lang.annotation.*;

/**
 * @author frank
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = {ElementType.TYPE})
@Documented
public @interface EnableSocket {
}

