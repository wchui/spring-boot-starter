package com.example.socket.filter.session;

import com.example.socket.core.Message;
import com.example.socket.core.Session;
import com.example.socket.handler.AbstractListener;
import com.example.socket.handler.Listener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 短连接SESSION监听器
 * @author Ramon
 */
public class TransientSessionListener extends AbstractListener {

    protected static final Logger logger = LoggerFactory.getLogger(TransientSessionListener.class);

    @Override
    public void received(Message message, Session session) {
        if (session == null || !(session instanceof TransientSession)) {
            return;
        }
        // 执行子监听器
        for (Listener listener : listeners) {
            listener.received(message, session);
        }
    }

    @Override
    public void sent(Message message, Session session) {
        if (session == null || !(session instanceof TransientSession)) {
            return;
        }
        // 执行子监听器
        for (Listener listener : listeners) {
            listener.sent(message, session);
        }
    }

    // ----------

    /** 消息过滤器 */
    private Queue<Listener> listeners = new LinkedBlockingQueue<>();

    public void setListeners(List<Listener> listeners) {
        this.listeners = new LinkedBlockingQueue<>(listeners);
    }

    public void addListener(Listener listener) {
        listeners.add(listener);
    }

}
