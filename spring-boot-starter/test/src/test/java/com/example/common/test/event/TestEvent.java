package com.example.common.test.event;

import com.example.event.Event;

/**
 * @author frank
 * @date 2019-09-20 17:46
 * @desc 事件体
 **/
public class TestEvent {

    /** 事件名*/
    public final static String NAME = "TestEvent";

    private String id;
    private String name;

    public static Event<TestEvent> valueOf(String id, String name) {
        TestEvent event = new TestEvent();
        event.id = id;
        event.name = name;
        return Event.valueOf(NAME, event);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "TestEvent{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
