package com.example.ramcache.orm.impl;

import org.hibernate.internal.StatelessSessionImpl;

/**
 * @author frank
 * session callback
 * @param <T>
 */
public interface Callback<T> {
	T call(StatelessSessionImpl session);
}