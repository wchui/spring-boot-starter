package com.example.ramcache.anno;

/**
 * 缓存单位
 * @author frank
 */
public enum CacheUnit {

	/** 实体 */
	ENTITY,
	/** 区域 */
	REGION;
}
