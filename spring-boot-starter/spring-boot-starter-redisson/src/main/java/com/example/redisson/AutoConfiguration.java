package com.example.redisson;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.SingleServerConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

/**
 * @author frank
 * @date 2019-10-12 14:28
 * @desc
 **/
@Configuration
public class AutoConfiguration {

    @Value("${spring.redis.host}")
    private String host;
    @Value("${spring.redis.port}")
    private String port;
    @Value("${spring.redis.password:}")
    private String password;

    @Bean
    public RedissonClient registerRedissonClient() {
        Assert.notNull(host);
        Assert.notNull(port);
        Config config = new Config();
        SingleServerConfig serverConfig = config.useSingleServer()
                .setAddress("redis://" + host + ":" + port);
        if (!StringUtils.isEmpty(password)) {
            serverConfig.setPassword(password);
        }
        return Redisson.create(config);
    }

    @Bean
    public RedisLock registerRedisLock() {
        return new RedisLockImpl(registerRedissonClient());
    }

}
