package com.example.socket.filter;

/**
 * @author frank
 * @date 2019-03-12 10:51
 * @desc 自定义拦截器排序
 **/
public interface FilterHandler {

    /** 没册名*/
    String getName();

    /** 排序*/
    int getOrder();
}
