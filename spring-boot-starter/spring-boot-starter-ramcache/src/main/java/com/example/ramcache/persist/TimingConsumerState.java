package com.example.ramcache.persist;

public enum TimingConsumerState {
	
	/** 执行 */
	RUNNING,
	/** 等待 */
	WAITING,
	/** 停止 */
	STOPPED;

}
