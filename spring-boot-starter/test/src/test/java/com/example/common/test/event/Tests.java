package com.example.common.test.event;

import com.example.event.Event;
import com.example.event.EventBus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author frank
 * @date 2019-09-20 16:23
 * @desc 发布事件
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class Tests {

    @Resource
    private EventBus eventBus;

    @Test
    public void post() {
        Event<TestEvent> event = TestEvent.valueOf("1", "小五");
        //发出异步事件
        eventBus.post(event);
        //发出同步事件
        eventBus.syncPost(event);
    }
}
