package com.example.socket.filter.session;

import javax.management.MXBean;

@MXBean
public interface NettySessionManagerMBean {

	/**
	 * 所有 SESSION 数量
	 */
	int getTotalCounts();

	/**
	 * 未验证的活动 SESSION 数量
	 */
	int getAnonymouCounts();

	/**
	 * 已验证的活动 SESSION 数量
	 */
	int getIdentitieCounts();

	/**
	 * 已验证的钝化 SESSION 数量
	 */
	int getPassivateCounts();

}