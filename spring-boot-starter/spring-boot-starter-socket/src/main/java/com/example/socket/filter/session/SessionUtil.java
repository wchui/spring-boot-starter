package com.example.socket.filter.session;

import com.example.socket.core.Session;
import io.netty.channel.Channel;

public interface SessionUtil {

    /**
     * 创建 SESSION
     * @param channel
     * @return
     */
    Session createSession(long id, Channel channel);
}