package com.example.socket.filter.session;

import com.example.socket.core.Request;
import com.example.socket.core.Session;

/**
 * 消息发送任务
 * @author Ramon
 */
public class WriteTask {

    public static WriteTask valueOf(Request<?> message, Session... sessions) {
        WriteTask e = new WriteTask();
        e.message = message;
        e.sessions = sessions;
        return e;
    }

    private Request<?> message;

    private Session[] sessions;

    public Request<?> getMessage() {
        return message;
    }

    public Session[] getSessions() {
        return sessions;
    }

}
