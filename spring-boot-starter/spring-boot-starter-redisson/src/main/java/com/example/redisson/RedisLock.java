package com.example.redisson;

import org.redisson.api.RLock;

import java.util.concurrent.TimeUnit;

public interface RedisLock {

    RLock lock(String lockKey);

    RLock lock(String lockKey, int timeout);

    RLock lock(String lockKey, TimeUnit unit, int timeout);

    boolean tryLock(String lockKey, int waitTime, int leaseTime);

    boolean tryLock(String lockKey, TimeUnit unit, int waitTime, int leaseTime);

    RLock unlock(String lockKey);

    RLock unlock(RLock lock);
}