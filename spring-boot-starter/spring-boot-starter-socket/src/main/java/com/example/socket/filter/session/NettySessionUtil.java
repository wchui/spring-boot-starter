package com.example.socket.filter.session;

import com.example.socket.core.Session;
import io.netty.channel.Channel;

public class NettySessionUtil implements SessionUtil {

    @Override
    public Session createSession(long id, Channel channel) {
        return new NettySession(id, channel);
    }
}