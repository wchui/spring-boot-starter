package com.example.socket.handler;

import com.example.socket.core.Request;
import com.example.socket.core.Session;
import com.example.socket.parameter.ResultCallback;

/**
 * 通信业务处理接口，是通信内容对应业务处理的抽象定义
 * @param I {@link Request#getBody()}类型
 * @param O {@link Response#getBody()}类型
 * @author frank
 */
public interface Processor<I, O> {

    /**
     * 处理通信请求
     * @param request 请求
     * @param session 通信会话
     * @param callback 异步回调
     */
    void process(Request<I> request, Session session, ResultCallback<O> callback);

}
