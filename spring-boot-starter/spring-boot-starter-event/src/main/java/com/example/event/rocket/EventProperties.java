package com.example.event.rocket;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author frank
 * @date 2019-11-11 21:09
 * @desc
 **/
@ConfigurationProperties(prefix = "event.mq")
@EnableScheduling
public class EventProperties {

    private String nameServer;
    private String topic = "DefaultRocketMqEventTopic";
    private String tag = "DefaultRocketMqEventTag";

    public String getNameServer() {
        return nameServer;
    }

    public void setNameServer(String nameServer) {
        this.nameServer = nameServer;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

}
