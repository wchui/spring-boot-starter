package com.example.socket.filter.session;

import com.example.socket.core.Request;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 发送代理执行器
 * @author Ramon
 * @param <T>
 */
public class PushProxyInvoker<T> extends ProxyInvokerSupport<T> implements InvocationHandler {

	/** 消息分发器 */
	private PushProxyFactory factory;

	PushProxyInvoker(PushProxyFactory factory) {
		this.factory = factory;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		Request<Object> request = buildRequest(method, args);
		factory.send(request);
		// 推送接口不接受返回值
		return null;
	}
}
