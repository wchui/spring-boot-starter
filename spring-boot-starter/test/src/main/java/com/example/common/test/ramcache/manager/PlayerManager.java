package com.example.common.test.ramcache.manager;

import com.example.ramcache.anno.Inject;
import com.example.ramcache.service.EntityCacheService;
import org.springframework.stereotype.Component;

@Component
public class PlayerManager {

    @Inject
    private EntityCacheService<Long, Player> testCace;

    public Player load(long id) {
        return testCace.loadOrCreate(id, id1 -> Player.valueOf(id));
    }

    public void update(Player test) {
        test.update("test-name-1");
    }

    public void delete(Player player) {
        testCace.clear(player.getId());
    }

    public void update(Player test, String name) {
        test.update(name);
    }
}