package com.example.event;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author frank
 * @date 2019-03-22 18:16
 * @desc
 **/
@ConfigurationProperties(prefix = "event")
public class ConfigProperties {
    private boolean disruptor = false;

    public boolean isDisruptor() {
        return disruptor;
    }

    public void setDisruptor(boolean disruptor) {
        this.disruptor = disruptor;
    }

}
