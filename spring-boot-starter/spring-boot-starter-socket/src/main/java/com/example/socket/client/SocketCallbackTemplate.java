package com.example.socket.client;

public abstract class SocketCallbackTemplate<T> implements SocketCallback<T> {

    @Override
    public void onError(Exception ex) {

    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onTimeout() {

    }
}