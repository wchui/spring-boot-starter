package com.example.socket.anno.impl;

import com.example.socket.anno.InRequest;
import com.example.socket.core.Request;
import com.example.socket.core.Session;
import com.example.socket.parameter.Parameter;
import com.example.socket.parameter.ParameterKind;
import com.example.socket.parameter.ResultCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;

public class InRequestParameter implements Parameter {

	private static final Logger logger = LoggerFactory.getLogger(InRequestParameter.class);

	private InRequest annotation;

	public static InRequestParameter valueOf(InRequest annotation) {
		InRequestParameter result = new InRequestParameter();
		result.annotation = annotation;
		return result;
	}

	@Override
	public ParameterKind getKind() {
		return ParameterKind.IN_REQUEST;
	}

	@Override
	public Object getValue(Request<?> request, Session session, ResultCallback<?> callback) {
		switch (annotation.value()) {
		case SN:
			return request.getSn();
		case COMMAND:
			return request.getCommand();
		case STATE:
			return request.getState();
		default:
			FormattingTuple message = MessageFormatter.format("无法处理的 InRequest 类型[{}]", annotation.value());
			logger.error(message.getMessage());
			throw new IllegalStateException(message.getMessage());
		}
	}

}
