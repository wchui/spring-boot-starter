package com.example.socket.filter.session;

import com.example.socket.core.Message;
import com.example.socket.core.Session;

import java.util.List;

/**
 * 短连接通信会话抽象接口，用于屏蔽底层的真实会话对象
 */
public interface TransientSession extends Session, TachSession{

	// ---------- 延迟推送 ----------

	/**
	 * 获取并清空缓存的推送消息
	 */
	List<Message> fetchDelayed();

	/**
	 * 加入延迟消息列表
	 * @param message
	 * @return 
	 */
	int addDelayed(Message message);

	// ---------- 消息历史 ----------

	/**
	 * 添加消息历史
	 * @param message
	 * @param capability
	 */
	void addHistory(Message message, int capability);

	/**
	 * 获取消息历史
	 * @param sn
	 * @return
	 */
	Message getHistory(int sn);

	/**
	 * 清除历史记录
	 */
	void clearHistory();
	
	/**获取当前session序列号*/
	int getSn();

	/**设置当前序列号*/
	void setSn(int sn);
}
