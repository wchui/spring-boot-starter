package com.example.common.test;

import com.example.console.Console;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestApplication {

    public static void main(String[] args) {
        Console console = new Console(TestApplication.class,args);
        console.start();
    }

}
