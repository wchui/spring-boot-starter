package com.example.ramcache.service;

import com.example.ramcache.IEntity;

@SuppressWarnings("rawtypes")
public interface EntityBuilder<PK extends Comparable, T extends IEntity<PK>> {

	T newInstance(PK id);

}
