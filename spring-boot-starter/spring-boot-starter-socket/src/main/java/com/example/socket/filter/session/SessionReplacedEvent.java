package com.example.socket.filter.session;

import com.example.socket.core.Session;

/**
 * 会话替换事件
 * @author frank
 */
public class SessionReplacedEvent extends SessionEvent {

    /** 被替换的会话 */
    private final Session prev;

    public SessionReplacedEvent(int cause, Object identity, Session session, Session prev) {
        super(cause, SessionEvent.Type.REPLACED, identity, session);
        this.prev = prev;
    }

    /**
     * 获取被替换的会话
     * @return
     */
    public Session getPrev() {
        return prev;
    }

}
