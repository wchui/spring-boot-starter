package com.example.socket.filter.transitory;

import com.example.socket.codec.MessageEncoder;
import com.example.socket.core.Message;
import com.example.socket.core.Session;
import com.example.socket.filter.session.TransientSession;
import com.example.socket.handler.AbstractListener;
import com.example.socket.handler.Listener;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.PooledByteBufAllocator;

import java.util.List;

/**
 * 消息历史过滤器 - 历史记录存储重发
 */
public class DelayedPushListener extends AbstractListener implements Listener {

    /** 编码器缓冲分配器 */
    private ByteBufAllocator alloc = new PooledByteBufAllocator();

    // -------------

    @Override
    public void sent(Message message, Session session) {

        if (!(session instanceof TransientSession)) {
            return;
        }
        //链接激活中
        boolean active = session.isConnected();

        //链接激活中
        if (active) {
            List<Message> delayeds = ((TransientSession) session).fetchDelayed();
            if (delayeds == null || delayeds.isEmpty()) {
                logger.debug("==== 空的SESSION[{}]延迟消息队列", session.getId());
                return;
            }
            // 缓存的延迟消息包, 附加到消息返回值
            for (Message msg : delayeds) {
                logger.info("<<<< 返回SESSION[{}]延迟消息[{}]", session.getId(), msg.getCommand());
                session.write(msg);
            }
        } else {//链接未激活 加入队列
            TransientSession transitorySession = (TransientSession) session;
            message.setIgnore(true);
            int size = transitorySession.addDelayed(message);
            logger.info("++++ 添加SESSION[{}]延迟推送队列[{}]信息 - {}", session.getId(), message.getCommand(), size);
        }

//		if (message.isResponse()) {
//			// 回应消息
//			List<Message> delayeds = ((TransientSession) session).fetchDelayed();
//			if (delayeds == null || delayeds.isEmpty()) {
//				if (logger.isDebugEnabled()) {
//					logger.debug("==== 空的SESSION[{}]延迟消息队列", new Object[] { session.getId() });
//				}
//				return;
//			}
//
//			// 缓存的延迟消息包, 附加到消息返回值
//			ByteBuf buffer = alloc.buffer();
//			for (Message msg : delayeds) {
//				MessageEncoder.encode(msg, buffer);
//				if (logger.isInfoEnabled()) {
//					logger.info("<<<< 返回SESSION[{}]延迟消息[{}]", new Object[] { session.getId(), msg.getCommand() });
//				}
//				session.write(msg);
//				buffer.clear();
//			}
//		} else {
//			TransientSession transitorySession = (TransientSession) session;
//			// 推送的消息
//			Command command = message.getCommand();
//			TypeDefinition definition = getDispatcher().getDefinition(command);
//			if (definition.isDelayed() && !active) {//是推送消息 并且链接未激活 加入队列
//				message.setIgnore(true);
//				int size = transitorySession.addDelayed(message);
//				if (logger.isInfoEnabled()) {
//					logger.info("++++ 添加SESSION[{}]延迟推送队列[{}]信息 - {}",
//							new Object[] { session.getId(), message.getCommand(), size });
//				}
//			}
//		}
    }
}
