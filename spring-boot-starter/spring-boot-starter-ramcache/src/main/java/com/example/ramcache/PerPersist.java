package com.example.ramcache;

/**
 * 每次持久化的预处理方法接口
 */
public interface PerPersist {

	/**
	 * 实体加载完成后的预处理方法
	 */
	void afterLoad();
	
	/**
	 * 实体持久化前的预处理方法
	 */
	void beforePersist();
}
