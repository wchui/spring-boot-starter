package com.example.socket.schema;

import com.example.socket.anno.SocketCommand;
import com.example.socket.anno.SocketModule;
import com.example.socket.core.Command;
import com.example.socket.handler.CommandRegister;
import com.example.socket.handler.MethodProcessor;
import com.example.socket.handler.ParameterBuilder;
import com.example.socket.properties.PackageScanProperties;
import com.example.socket.utils.AnnotationUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;
import org.springframework.beans.BeansException;
import org.springframework.beans.FatalBeanException;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.util.ClassUtils;
import org.springframework.util.SystemPropertyUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 指令注册器工厂
 * @author frank
 */

public class RegisterFactory implements FactoryBean<CommandRegister>, BeanPostProcessor {
    private static final String DEFAULT_RESOURCE_PATTERN = "**/*.class";
    private static final Logger logger = LoggerFactory.getLogger(RegisterFactory.class);
    private final String NAME = "socketCommandRegister";
    private ParameterBuilder builder;
    /** 扫描指令(整个包)*/
    private List<String> packages;
    /** 扫描指令(单个Class)*/
    private List<String> interfaces;
    /** 排除的class*/
    private List<String> excludes;
    /** 含有*号通配符*/
    private List<String> includes;

    /** 资源搜索分析器，由它来负责检索{@link SocketModule}声明的接口 */
    private ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
    /** 类的元数据读取器，由它来负责读取类上的注释信息 */
    private MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(this.resourcePatternResolver);

    @PostConstruct
    void initialize() throws Exception {
        boolean skipScan = packages.isEmpty() && interfaces.isEmpty() && includes.isEmpty();
        if (skipScan) {
            return;
        }
        List<Resource> resources = findResources();
        for (Resource resource : resources) {
            if (!resource.isReadable()) {
                //不可读资源，忽略
                continue;
            }
            // 判断是否有 SocketModule 注释
            MetadataReader metadataReader = this.metadataReaderFactory.getMetadataReader(resource);
            AnnotationMetadata annoData = metadataReader.getAnnotationMetadata();
            if (!annoData.hasAnnotation(SocketModule.class.getName())) {
                // 不是要处理的资源，忽略
                continue;
            }
            ClassMetadata clzMeta = metadataReader.getClassMetadata();
            // 只处理接口
            if (!clzMeta.isInterface()) {
                continue;
            }
            Class<?> clz = Class.forName(clzMeta.getClassName());
            getObject().registerInterface(clz);
        }

        // 直接接口注册
        if (interfaces != null) {
            for (String inf : interfaces) {
                Class<?> clz = Class.forName(inf);
                getObject().registerInterface(clz);
            }
        }

    }


    /**
     * 查找出全部的类资源
     * @return
     * @throws IOException
     */
    private List<Resource> findResources() throws IOException {
        List<Resource> resources = new ArrayList<>();
        for (String name : packages) {
            String path = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + resolveBasePackage(name) + "/"
                    + DEFAULT_RESOURCE_PATTERN;
            Resource[] tmp = resourcePatternResolver.getResources(path);
            Collections.addAll(resources, tmp);
        }
        for (String name : includes) {
            String path = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + resolveBasePackage(name) + ".class";
            Resource[] tmp = resourcePatternResolver.getResources(path);
            Collections.addAll(resources, tmp);
        }
        List<Resource> removed = new ArrayList<>();
        for (String name : excludes) {
            String path = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + resolveBasePackage(name) + ".class";
            System.out.println(path);
            Resource[] tmp = resourcePatternResolver.getResources(path);
            for (Resource resource : resources) {
                for (Resource target : tmp) {
                    if (target.getURL().getPath().equals(resource.getURL().getPath())) {
                        removed.add(resource);
                    }
                }
            }
            resources.removeAll(removed);
        }
        return resources;
    }

    public RegisterFactory(ParameterBuilder builder, PackageScanProperties properties) {
        this.builder = builder;
        this.packages = properties.getPackages();
        this.interfaces = properties.getInterfaces();
        this.excludes = properties.getExcludes();
        this.includes = properties.getInclueds();
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Class<?> clazz = AnnotationUtility
                .findAnnotationDeclaringClassAndInterface(SocketModule.class, bean.getClass());
        if (clazz == null) {
            return bean;
        }

        SocketModule handler = clazz.getAnnotation(SocketModule.class);
        if (handler == null) {
            return bean;
        }
        for (Method method : clazz.getMethods()) {
            SocketCommand socketCommand = AnnotationUtility.findAnnotation(method, SocketCommand.class);
            if (socketCommand == null) {
                // 不是指令方法，跳过不处理
                continue;
            }
            Command command = Command.valueOf(handler.value(), socketCommand.value());
            // 进行注册
            try {
                // 创建指令对象
                if (getObject().contain(command)) {
                    //指令已经注册
                    continue;
                }
                MethodProcessor processor = MethodProcessor.valueOf(handler.format(), clazz, bean, method, builder);
                getObject().register(command, processor.getDefinition(), processor);
            } catch (Exception e) {
                FormattingTuple message = MessageFormatter.format("注册指令[{}]到对应方法[{}]时失败", command, method);
                logger.error(message.getMessage());
                throw new FatalBeanException(message.getMessage(), e);
            }
        }
        return bean;
    }

    // 实现 Spring 接口的方法

    private CommandRegister commandRegister;

    @Override
    public CommandRegister getObject() throws Exception {
        if (commandRegister == null) {
            commandRegister = new CommandRegister(NAME, builder);
        }
        return commandRegister;
    }

    @Override
    public Class<?> getObjectType() {
        return CommandRegister.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    protected String resolveBasePackage(String basePackage) {
        return ClassUtils.convertClassNameToResourcePath(SystemPropertyUtils.resolvePlaceholders(basePackage));
    }
    // Getter and Setter ...

    public ParameterBuilder getBuilder() {
        return builder;
    }

    public void setBuilder(ParameterBuilder builder) {
        this.builder = builder;
    }
}
