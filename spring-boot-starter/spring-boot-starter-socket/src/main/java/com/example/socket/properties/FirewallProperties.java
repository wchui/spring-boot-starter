package com.example.socket.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author frank
 * @date 2019-03-12 9:52
 * @desc
 **/
@ConfigurationProperties(prefix = "socket.server.firewall")
public class FirewallProperties {

    /** firewall white list*/
    private String allows = "127.0.0.1,192.168.*.*";
    /** firewall blacklist*/
    private String blocks;
    /** block all user on server started */
    private Boolean autostartBlock = false;
    /** temporary blacklist time*/
    private int blockTimes = 300;
    /** max client*/
    private int maxClient = 65535;
    /** the number of bytes received per second*/
    private int recvBytesLimitPerSecond = 8192;
    /** number of packets received per second*/
    private int recvPackagesLimitPerSecond = 128;
    /** number of violations allowed*/
    private int faultTolerant = 10;


    public String getAllows() {
        return allows;
    }

    public void setAllows(String allows) {
        this.allows = allows;
    }

    public String getBlocks() {
        return blocks;
    }

    public void setBlocks(String blocks) {
        this.blocks = blocks;
    }

    public Boolean getAutostartBlock() {
        return autostartBlock;
    }

    public void setAutostartBlock(Boolean autostartBlock) {
        this.autostartBlock = autostartBlock;
    }

    public int getBlockTimes() {
        return blockTimes;
    }

    public void setBlockTimes(int blockTimes) {
        this.blockTimes = blockTimes;
    }

    public int getMaxClient() {
        return maxClient;
    }

    public void setMaxClient(int maxClient) {
        this.maxClient = maxClient;
    }

    public int getRecvBytesLimitPerSecond() {
        return recvBytesLimitPerSecond;
    }

    public void setRecvBytesLimitPerSecond(int recvBytesLimitPerSecond) {
        this.recvBytesLimitPerSecond = recvBytesLimitPerSecond;
    }

    public int getRecvPackagesLimitPerSecond() {
        return recvPackagesLimitPerSecond;
    }

    public void setRecvPackagesLimitPerSecond(int recvPackagesLimitPerSecond) {
        this.recvPackagesLimitPerSecond = recvPackagesLimitPerSecond;
    }

    public int getFaultTolerant() {
        return faultTolerant;
    }

    public void setFaultTolerant(int faultTolerant) {
        this.faultTolerant = faultTolerant;
    }
}
