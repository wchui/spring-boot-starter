package com.example.socket.handler;

import com.example.socket.core.Message;
import com.example.socket.core.Session;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ListenerSupport {

	private final List<Listener> listeners = new CopyOnWriteArrayList<Listener>();

	public void addListener(Listener listener) {
		this.listeners.add(listener);
	}
	
	public void addListener(int index, Listener listener) {
		this.listeners.add(index, listener);
	}

	public void removeListener(Listener listener) {
		this.listeners.remove(listener);
	}

	public void fireSentMessage(Message message, Session sessions) {
		for (Listener listener : listeners) {
			listener.sent(message, sessions);
		}
	}

	public void fireReceivedMessage(Message message, Session sessions) {
		for (Listener listener : listeners) {
			listener.received(message, sessions);
		}
	}
}
