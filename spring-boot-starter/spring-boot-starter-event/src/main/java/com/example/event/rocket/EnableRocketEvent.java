package com.example.event.rocket;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(RocketAutoConfigure.class)
@Documented
public @interface EnableRocketEvent {

}