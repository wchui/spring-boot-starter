package com.example.common.test.event;

import com.example.event.AbstractReceiver;
import org.springframework.stereotype.Component;

/**
 * @author frank
 * @date 2019-09-20 16:22
 * @desc 事件接收器(订阅者)
 **/
@Component
public class TestReceiver extends AbstractReceiver<TestEvent> {

    @Override
    public String[] getEventNames() {
        //订阅的事件名（一个接收器可订阅多个事件）
        return new String[]{TestEvent.NAME};
    }

    /**
     * 处理事件
     * @param event 事件消息体
     */
    @Override
    public void doEvent(TestEvent event) {
        System.out.println("收到事件:"+event+"当前线程:"+Thread.currentThread().getName());
    }
}
