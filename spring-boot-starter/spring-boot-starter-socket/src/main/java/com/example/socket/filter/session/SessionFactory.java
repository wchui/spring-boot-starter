package com.example.socket.filter.session;

import com.example.socket.core.Session;

import java.util.Collection;

public interface SessionFactory<T> {

	/**
	 * 获取SESSION
	 * @param id
	 * @return
	 */
	Session getSession(long id);

	/**
	 * 创建 SESSION
	 * @param channel
	 * @return
	 */
	Session createSession(T channel, SessionUtil creator);

	/**
	 * 销毁SESSION
	 * @param id
	 * @return
	 */
	void destorySession(Session session);

	/**
	 * 获取全部SESSION
	 * @return
	 */
	Collection<Session> getAll();

	void addSession(Session session);

}