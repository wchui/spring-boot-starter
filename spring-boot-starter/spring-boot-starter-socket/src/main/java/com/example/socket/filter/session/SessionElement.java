package com.example.socket.filter.session;

import java.util.Objects;

public class SessionElement {
    /** session id */
    private long id;
    /**玩家标识*/
    private Object identity;

    /**
     *
     * @param type 1 新建session 2钝化session
     * @param id
     * @return
     */
    public static SessionElement valueOf(long id) {
        SessionElement element = new SessionElement();
        element.id = id;
        return element;
    }

    /**
     *
     * @param type 1 新建session 2钝化session
     * @param id
     * @return
     */
    public static SessionElement valueOf(long id, Object identity) {
        SessionElement element = new SessionElement();
        element.id = id;
        element.identity = identity;
        return element;
    }

    public long getId() {
        return id;
    }

    public Object getIdentity() {
        return identity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SessionElement that = (SessionElement) o;
        return id == that.id &&
                Objects.equals(identity, that.identity);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, identity);
    }
}