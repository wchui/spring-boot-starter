package com.example.socket.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * @author frank
 * @date 2019-03-13 16:40
 * @desc
 **/
@ConfigurationProperties(prefix = "socket.server.scan-command")
public class PackageScanProperties {
    /** scan by packages*/
    private List<String> packages = new ArrayList<>();
    /** scan by interface*/
    private List<String> interfaces = new ArrayList<>();
    /** exclude interface*/
    private List<String> excludes = new ArrayList<>();
    /** include [*] expression*/
    private List<String> inclueds = new ArrayList<>();
    public List<String> getPackages() {
        return packages;
    }

    public void setPackages(List<String> packages) {
        this.packages = packages;
    }

    public List<String> getInterfaces() {
        return interfaces;
    }

    public void setInterfaces(List<String> interfaces) {
        this.interfaces = interfaces;
    }

    public List<String> getExcludes() {
        return excludes;
    }

    public void setExcludes(List<String> excludes) {
        this.excludes = excludes;
    }

    public List<String> getInclueds() {
        return inclueds;
    }

    public void setInclueds(List<String> inclueds) {
        this.inclueds = inclueds;
    }
}
