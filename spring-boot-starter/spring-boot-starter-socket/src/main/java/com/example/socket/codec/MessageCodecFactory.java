package com.example.socket.codec;

/**
 * 通信消息({@link Message})对象编码工厂
 * @author Ramon
 */
public class MessageCodecFactory {

	public static final String NAME = "codec";

	/**
	 * 消息编码器
	 */
	public MessageEncoder getEncoder() {
		return new MessageEncoder();
	}

	/**
	 * 消息解码器
	 */
	public MessageDecoder getDecoder(int maxLen) {
		return new MessageDecoder(maxLen);
	}
}
