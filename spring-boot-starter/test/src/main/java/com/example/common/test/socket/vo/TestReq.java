package com.example.common.test.socket.vo;

import com.baidu.bjf.remoting.protobuf.annotation.Protobuf;
import com.baidu.bjf.remoting.protobuf.annotation.ProtobufClass;

@ProtobufClass
public class TestReq {
    @Protobuf
    private int id;
    @Protobuf
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "TestReq{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
