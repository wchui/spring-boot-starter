package com.example.socket.handler;


import com.example.socket.core.Message;
import com.example.socket.core.Session;

/**
 * 消息事件监听器
 * @author Ramon
 */
public interface Listener {

	/** 消息分发器 */
	void setDispatcher(Dispatcher dispatcher);

	/**
	 * 处理接收的消息
	 * @param message
	 * @param session
	 */
	void received(Message message, Session session);

	/**
	 * 向指定会话发送通信信息
	 * @param request
	 * @param sessions
	 */
	void sent(Message message, Session session);

}
