package com.example.console;

/***
 * @author frank
 */
public interface ShutdownHook {
    /**
     * 关闭容器
     */
    void shutdown();
}
