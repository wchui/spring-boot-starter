package com.example.socket.handler;

import com.example.socket.core.*;

/**
 * 通信分发器接口
 * @author Ramon
 */
public interface Dispatcher {

	// ----- 消息定义 -----

	/**
	 * 获取指令定义
	 * @param command
	 * @return
	 */
	TypeDefinition getDefinition(Command command);

	/**
	 * 获取指令处理器
	 * @param command
	 * @return
	 */
	Processor<?, ?> getProcessor(Command command);

	/**
	 * 注册指令处理器
	 * @param command
	 * @param definition
	 * @param processor
	 */
	void register(Command command, TypeDefinition definition, Processor<?, ?> processor);

	/**
	 * 注册接口指令类型
	 * @param clazz
	 */
	void registerInterface(Class<?> clazz);

	// ----- 监听器 -----

	/**
	 * 添加监听器
	 * @param listener
	 */
	void addListener(Listener listener);
	
	/**
	 * 添加监听器
	 * @param listener
	 */
	void addListener(int index, Listener listener);

	/**
	 * 移除监听器
	 * @param listener
	 */
	void removeListener(Listener listener);

	// ----- 消息处理 -----

	/**
	 * Request -> Meessage
	 */
	Message encodeRequest(Request<?> request);

	/**
	 * Response -> Meessage
	 */
	Message encodeResonse(Response<?> response);

	/**
	 * Message -> Response
	 */
	Response<?> decodeResponse(Message message);

	/**
	 * Message -> Request
	 */
	Request<?> decodeRequest(Message message);
	// ----

	/**
	 * 接收到指定会话的通信信息监听
	 * @param message
	 * @param session
	 */
	void receive(Message message, Session session);
	/**
	 * 向指定会话发送通信信息监听
	 * @param message
	 * @param sessions
	 */
	void send(Message message, Session sessions);

	/**
	 * 会话销毁监听
	 * @param session
	 */
	void closed(Session session);

}
