package com.example.event.rocket;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author frank
 * @date 2019-09-20 17:32
 * @desc
 **/

@Configuration
public class RocketAutoConfigure implements ImportBeanDefinitionRegistrar {

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        //注册事件bus
        BeanDefinitionBuilder rocketImplBuilder = BeanDefinitionBuilder.genericBeanDefinition(RemoteEventBus.class);
        registry.registerBeanDefinition("rocketRemoteEvent", rocketImplBuilder.getBeanDefinition());
        //注册消费者
        BeanDefinitionBuilder rocketMqConsumerBuilder = BeanDefinitionBuilder.genericBeanDefinition(RocketMqConsumer.class);
        registry.registerBeanDefinition("rocketMqEventConsumer", rocketMqConsumerBuilder.getBeanDefinition());
        //注册生产者
        BeanDefinitionBuilder rocketMqProducerBuilder = BeanDefinitionBuilder.genericBeanDefinition(RocketMqProducer.class);
        registry.registerBeanDefinition("rocketMqEventProducer", rocketMqProducerBuilder.getBeanDefinition());
    }

}