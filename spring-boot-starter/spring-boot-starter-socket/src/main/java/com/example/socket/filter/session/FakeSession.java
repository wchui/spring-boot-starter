package com.example.socket.filter.session;

import com.example.socket.core.Session;
import com.example.socket.core.SessionContext;

import java.net.SocketAddress;
import java.util.concurrent.Future;

public class FakeSession implements Session {
	
	/**目标玩家id*/
	private long target;

	@Override
	public long getId() {
		return 0;
	}
	
	public void setId(long id){
		
	}

	@Override
	public SessionContext getContext() {
		return null;
	}

	@Override
	public boolean isConnected() {
		return true;
	}

	@Override
	public SocketAddress getRemoteAddress() {
		return null;
	}

	@Override
	public Future<?> close() {
		return null;
	}

	@Override
	public Future<?> write(Object msg) {
		return null;
	}

	@Override
	public void destory() {

	}

	@Override
	public boolean isTransitory() {
		return true;
	}

	public long getTarget() {
		return target;
	}

	public void setTarget(long target) {
		this.target = target;
	}

	public static FakeSession valueOf(Object id) {
		FakeSession result = new FakeSession();
		result.target =(Long) id;
		return result;
	}

	@Override
	public SessionContext bindContext(SessionContext context) {
		return context;
	}

	@Override
	public void setTransitory(boolean transitory) {
	}

	@Override
	public boolean isDestroyed() {
		return false;
	}

	@Override
	public long getLastTime() {
		// TODO Auto-generated method stub
		return 0;
	}
}