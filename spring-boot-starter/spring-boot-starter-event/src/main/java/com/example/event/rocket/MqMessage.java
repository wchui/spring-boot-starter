package com.example.event.rocket;

public class MqMessage {
    private String topic;

    private String tag;

    private String key;

    private Object body;

    public static MqMessage valueOf(String topic, String tags, Object body) {
        return valueOf(topic, tags, body, "");
    }

    public static MqMessage valueOf(String topic, String tags, Object body, String key) {
        MqMessage mqMessage = new MqMessage();
        mqMessage.topic = topic;
        mqMessage.tag = tags;
        mqMessage.key = key;
        mqMessage.body = body;
        return mqMessage;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }
}