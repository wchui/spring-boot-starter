package com.example.socket.filter.transitory;

import com.example.socket.core.Message;
import com.example.socket.core.Session;
import com.example.socket.filter.session.TransientSession;
import com.example.socket.handler.AbstractListener;

public class SnCheckListener extends AbstractListener {

	@Override
	public void received(Message message, Session session) {
		
		if(session instanceof TransientSession){
			TransientSession tSession = (TransientSession) session;
			int sn = tSession.getSn();
			if(sn >= message.getSn()){
				message.setIgnore(true);
				logger.info("SESSION[{}]收到已处理消息序列号消息[{}]设置忽略", session.getId(), sn);
			}else {
				tSession.setSn(sn);
			}
		}
	}
}