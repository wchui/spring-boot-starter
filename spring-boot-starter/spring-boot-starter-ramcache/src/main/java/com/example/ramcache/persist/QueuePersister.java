package com.example.ramcache.persist;

import com.example.ramcache.IEntity;
import com.example.ramcache.exception.ConfigurationException;
import com.example.ramcache.exception.StateException;
import com.example.ramcache.orm.Accessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;

/**
 * @author frank
 * 队列持久化处理器<br/>
 * 允许选择是否开启重复更新抑制
 */
@SuppressWarnings("rawtypes")
public class QueuePersister implements Persister {

	private static final Logger logger = LoggerFactory.getLogger(QueuePersister.class);
	
	private static final String SPLIT = ":";

	/** 名称 */
	private String name;
	/** 更新队列 */
	private BlockingQueue<String> queue;
	/** 对应实体的处理监听器 */
	private ConcurrentHashMap<Class<? extends IEntity>, Listener> listeners = new ConcurrentHashMap<>();
	/** 初始化标识 */
	private boolean initialize;
	/** 抑制重复更新状态 */
	@SuppressWarnings("unused")
	private boolean flag;
	/** 正在等待更新的信息缓存 */
	private ConcurrentMap<String, Element> elements = new ConcurrentHashMap<>();

	/** 消费线程 */
	private QueueConsumer consumer;

	/** 初始化方法 */
	@Override
	public synchronized void initialize(String name, Accessor accessor, String config) {
		if (initialize) {
			throw new ConfigurationException("重复初始化异常");
		}
		Assert.notNull(accessor, "持久层数据访问器不能为 null");

		try {
			String[] array = config.split(SPLIT);
			int size = Integer.parseInt(array[0]);
			if (size >= 0) {
				this.queue = new ArrayBlockingQueue<>(size);
			} else {
				this.queue = new LinkedBlockingQueue<>();
			}
			
			this.flag = Boolean.parseBoolean(array[1]);
			this.name = name;
			this.consumer = new QueueConsumer(name, accessor, this.queue, this);
			this.initialize = true;
		} catch (Exception e) {
			throw new ConfigurationException("持久化处理器[" + name + "]初始化异常:" + e.getMessage());
		}
	}
	
	/** 添加监听器 */
	@Override
	public void addListener(Class<? extends IEntity> clz, Listener listener) {
		if (listener == null) {
			throw new ConfigurationException("被添加的监听器实例不能为空");
		}
		listeners.put(clz, listener);
	}
	
	/**
	 * 将指定元素插入此队列中，将等待可用的空间（如果有必要）。
	 * @param element 被添加元素(元素为null时直接返回)
	 */
	@Override
	public void put(Element element) {
		if (element == null) {
			return;
		}
		if (stop) {
			FormattingTuple message = MessageFormatter.format("实体更新队列[{}]已经停止,更新元素[{}]将不被接受", name, element);
			logger.error(message.getMessage());
			throw new StateException(message.getMessage());
		}
		String id = element.getIdentity();
		try {
			Element prev = elements.remove(id);
			
			// 更新元素不存在的场景
			if (prev == null) {
				elements.put(id, element);
				queue.put(id);
				return;
			}
			queue.remove(id);
			// 更新元素已经存在的场景
			EventType prevType = prev.getType();
			if (prev.update(element)) {
				// 当从REMOVE合并为UPDATE的时候要让监听器通知缓存服务将内部的临时失效主键清除
				if (prevType == EventType.REMOVE && prev.getType() == EventType.UPDATE) {
					Listener listener = getListener(element.getEntityClass());
					if (listener != null) {
						listener.notify(EventType.REMOVE, true, prev.getId(), null, null);
					}
				}
				elements.put(id, prev);
				queue.put(id);
			}
		} catch (InterruptedException e) {
			// 这种情况是不应该会出现的
			logger.error("等待将元素[{}]添加到队列时被打断", new Object[] { element, e });
		}
	}

	@Override
	public Listener getListener(Class<? extends IEntity> clz) {
		return listeners.get(clz);
	}
	
	@Override
	public Map<String, String> getInfo() {
		HashMap<String, String> result = new HashMap<>();
		result.put("size", Integer.toString(size()));
		result.put("error", Integer.toString(consumer.getError()));
		return result;
	}
	
	public Element remove(String identity) {
		return elements.remove(identity);
	}

	/**
	 * 获取队列中的元素数量
	 * @return
	 */
	public int size() {
		return queue.size();
	}

	/** 停止状态 */
	private volatile boolean stop;

	/** 停止更新队列并等待全部*/
	@Override
	public void shutdown() {
		if(logger.isDebugEnabled()) {
			logger.debug("停机回写队列[{}]开始...", name);
		}
		stop = true;
		for (;;) {
			if (queue.isEmpty()) {
				break;
			}
			Thread.yield();
		}
		logger.error("停机回写队列[{}]完成...", name);
	}

	@Override
	public Element get(String key) {
		return elements.get(key);
	}

}

