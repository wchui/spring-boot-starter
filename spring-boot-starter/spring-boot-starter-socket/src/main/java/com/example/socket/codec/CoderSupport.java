package com.example.socket.codec;

import com.example.socket.exception.DecodeException;
import com.example.socket.exception.EncodeException;
import com.example.socket.handler.CommandInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;

/**
 * 抽象编解码器，负责封装处理异常
 * @author frank
 */
public abstract class CoderSupport implements Coder {

	protected Logger logger=LoggerFactory.getLogger(getClass());
	
	@Override
	public byte[] encode(Object obj, Type type) {
		try {
			return doEncode(obj, type);
		} catch (Exception e) {
			throw new EncodeException(e);
		}
	}

	@Override
	public Object decode(byte[] bytes, Type type) {
		try {
			return doDecode(bytes, type);
		} catch (Exception e) {
			throw new DecodeException(e);
		}
	}

	/**
	 * 等价于{@link Coder#encode(Object, Object)},子类实现该方法完成编码工作
	 * @param obj
	 * @param type
	 * @return
	 */
	protected abstract byte[] doEncode(Object obj, Type type);
	
	/**
	 * 等价于{@link Coder#decode(byte[], Object)},子类实现该方法完成解码工作
	 * @param bytes
	 * @param type
	 * @return
	 */
	protected abstract Object doDecode(byte[] bytes, Type type);

	@Override
	public void afterRegister(CommandInfo commandInfo) {
		// do nothing
	}
	
}
