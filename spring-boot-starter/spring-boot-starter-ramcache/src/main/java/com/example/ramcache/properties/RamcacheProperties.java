package com.example.ramcache.properties;

import com.example.ramcache.anno.CachedSizes;
import com.example.ramcache.anno.Persisters;
import com.example.ramcache.persist.PersisterType;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author frank
 * @date 2019-03-22 18:16
 * @desc
 **/
@ConfigurationProperties(prefix = "ramcache")
public class RamcacheProperties {

    private long shutdownDelay = 5000;

    private Map<String, Integer> constants = new HashMap<>();

    private Set<PersisterProperties> persisters = new HashSet<>();


    public void initilizer() {

    }

    public long getShutdownDelay() {
        return shutdownDelay;
    }

    public void setShutdownDelay(long shutdownDelay) {
        this.shutdownDelay = shutdownDelay;
    }

    public Map<String, Integer> getConstants() {
        Map<String, Integer> ramcacheConstants = getDefaultRamcacheConstants();
        ramcacheConstants.putAll(this.constants);
        return ramcacheConstants;
    }

    public void setConstants(Map<String, Integer> constants) {
        this.constants = constants;
    }

    public Set<PersisterProperties> getPersisters() {
        Set<PersisterProperties> defaults = getDefaultPersisters();
        defaults.addAll(this.persisters);
        return defaults;
    }

    public void setPersisters(Set<PersisterProperties> persisters) {
        this.persisters = persisters;
    }

    /**
     * 默认
     * @return
     */
    public Map<String, Integer> getDefaultRamcacheConstants() {
        Map<String, Integer> ramcacheConstants = new HashMap<>();
        ramcacheConstants.put(CachedSizes.DEFAULT, 5000);
        ramcacheConstants.put(CachedSizes.DOUBLE, 10000);
        ramcacheConstants.put(CachedSizes.TRIPLE, 15000);
        ramcacheConstants.put(CachedSizes.TENTH, 50000);
        ramcacheConstants.put(CachedSizes.MAXIMUM, 300000);
        return ramcacheConstants;
    }

    public Set<PersisterProperties> getDefaultPersisters() {
        Set<PersisterProperties> persisters = new HashSet<>();
        persisters.add(PersisterProperties.valueOf(Persisters.PRE_MINUTE, "0 0/1 * * * *", PersisterType.TIMING));
        persisters.add(PersisterProperties.valueOf(Persisters.PRE_5_MINUTE, "8 0/5 * * * *", PersisterType.TIMING));
        persisters.add(PersisterProperties.valueOf(Persisters.PRE_10_MINUTE, "20 0/10 * * * *", PersisterType.TIMING));
        persisters.add(PersisterProperties.valueOf(Persisters.PRE_HALF_HOUR, "36 0/30 * * * *", PersisterType.TIMING));
        persisters.add(PersisterProperties.valueOf(Persisters.PRE_HOUR, "53 0 0/1 * * *", PersisterType.TIMING));
        return persisters;
    }
}
