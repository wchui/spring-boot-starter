package com.example.socket.exception;

/**
 * 业务模块转发异常
 * @author yangjin
 * @date 2015年11月18日 下午3:31:48
 */
public class ErrorForwardException extends RuntimeException {
	private static final long serialVersionUID = -4597641573824058256L;
	
	/**模块号*/
	private short module;
	/**方法号*/
	private short command;
	/**消息体*/
	private Object body;
	
	public short getModule(){
		return module;
	}
	
	public short getCommand(){
		return command;
	}
	
	public Object getBody(){
		return body;
	}
	
	public static ErrorForwardException valueOf(short module, short command, Object body){
		ErrorForwardException result = new ErrorForwardException();
		result.module = module;
		result.command = command;
		result.body = body;
		return result;
	}
}