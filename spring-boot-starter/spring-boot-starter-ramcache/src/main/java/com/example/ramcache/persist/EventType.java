package com.example.ramcache.persist;

/**
 * 实体更新事件类型定义
 * @author frank
 */
public enum EventType {

	/** 插入 */
	SAVE,
	/** 更新 */
	UPDATE,
	/** 删除 */
	REMOVE;
	
}
