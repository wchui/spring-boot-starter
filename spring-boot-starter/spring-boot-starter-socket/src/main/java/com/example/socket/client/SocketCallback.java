package com.example.socket.client;

/**
 * Soket请求回调
 * @param <T>
 */
public interface SocketCallback<T> {

	/**
	 * 成功返回
	 */
	void onSuccess(T response);

	/**
	 * 异常捕捉
	 */
	void onError(Exception ex);

	/**
	 * 取消清求
	 */
	void onCancel();
	
	/**
	 * 超时
	 */
	void onTimeout();
}
