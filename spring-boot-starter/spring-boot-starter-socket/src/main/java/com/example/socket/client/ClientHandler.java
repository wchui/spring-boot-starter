package com.example.socket.client;

import com.example.socket.handler.Dispatcher;
import com.example.socket.handler.NettyHandler;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;

import java.util.concurrent.ExecutorService;

/**
 * 客户端handler
 * @author yangjin
 */
@Sharable
public class ClientHandler extends NettyHandler {
	
	protected ExecutorService executor;

	public ClientHandler(Dispatcher dispatcher, ExecutorService executor) {
		super(dispatcher);
		this.executor = executor;
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		executor.submit(new InterReadWork(ctx, msg));
	}
	
	class InterReadWork implements Runnable{
		private ChannelHandlerContext ctx;
		private Object msg;
		
		public InterReadWork(ChannelHandlerContext ctx, Object msg) {
			this.ctx = ctx;
			this.msg = msg;
		}

		@Override
		public void run() {
			try {
				ClientHandler.super.channelRead(ctx, msg);
			} catch (Exception e) {
				
			}
		}
	}
}