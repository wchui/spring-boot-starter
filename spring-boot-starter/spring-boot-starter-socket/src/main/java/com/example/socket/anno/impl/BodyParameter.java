package com.example.socket.anno.impl;

import com.example.socket.core.Request;
import com.example.socket.core.Session;
import com.example.socket.parameter.Parameter;
import com.example.socket.parameter.ParameterKind;
import com.example.socket.parameter.ResultCallback;

public class BodyParameter implements Parameter {

    public static final BodyParameter instance = new BodyParameter();

    @Override
    public ParameterKind getKind() {
        return ParameterKind.BODY;
    }

    @Override
    public Object getValue(Request<?> request, Session session, ResultCallback<?> callback) {
        return request.getBody();
    }

}
