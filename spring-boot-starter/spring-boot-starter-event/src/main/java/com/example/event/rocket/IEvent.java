package com.example.event.rocket;

import org.springframework.context.ApplicationEvent;

/**
 * @author frank
 * @date 2019-11-11 21:36
 * @desc
 **/
public abstract class IEvent extends ApplicationEvent {
    public IEvent() {
        super(new Object());
    }
}
